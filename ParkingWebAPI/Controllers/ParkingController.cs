﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ParkingSystem.Abstractions;
using ParkingSystem.Exceptions;
using ParkingSystem.Models;

namespace ParkingWebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        Parking parking;

        public ParkingController(Parking parking)
        {
            this.parking = parking;
        }

        // GET api/parking/balance
        [HttpGet]
        [Route("balance")]
        public ActionResult<decimal> GetBalance()
        {
            return this.parking.Balance;
        }

        // GET api/parking/earnings
        [HttpGet]
        [Route("earnings")]
        public ActionResult<decimal> GetEarnings()
        {
            return this.parking.TransactionService.Earnings();
        }

        // GET api/parking/places
        [HttpGet]
        [Route("places")]
        public ActionResult<(int freePlaces, int occupiedPlaces)> GetPlacesInfo()
        {
            int occupiedPlaces = this.parking.VehicleList.Count;
            int freePlaces = this.parking.Capacity - occupiedPlaces;
            return (freePlaces, occupiedPlaces);
        }

        // GET api/parking/transactions/last
        [HttpGet]
        [Route("transactions/last")]
        public ActionResult<List<string>> GetLastTransactions()
        {
            List<string> transactionsInfo = new List<string>();
            if (this.parking.TransactionService.Transactions == null)
            {
                transactionsInfo.Add("There is no transactions yet.");
            }
            else
            {
                foreach (Transaction t in this.parking.TransactionService.Transactions)
                {
                    transactionsInfo.Add(t.ToString());
                }
            }

            return transactionsInfo;
        }

        // GET api/parking/transactions/all
        [HttpGet]
        [Route("transactions/all")]
        public ActionResult<string[]> GetAllTransactions()
        {
            return this.parking.TransactionService.GetAllTransactions();
        }

        // GET api/parking/vehicle/all
        [HttpGet]
        [Route("vehicle/all")]
        public ActionResult<List<string>> GetAllVehicles()
        {
            var vehicleList = this.parking.VehicleList;

            List<string> vehicleStringList = new List<string>();
            foreach (var vehicle in vehicleList)
            {
                vehicleStringList.Add(vehicle.ToString());
            }

            return vehicleStringList;
        }

        // DELETE: api/parking/vehicle/2
        [HttpDelete("vehicle/{id}")]
        // [Route("vehicle")]
        public ActionResult Delete(int id)
        {
            try
            {
                this.parking.RemoveVehicle(id);
                return (ActionResult)Ok();
            }
            catch (InvalidVehicleIdException)
            {
                return (ActionResult)BadRequest();
            }
        }

        // PUT: api/parking/vehicle/2
        [HttpPut("vehicle/{typeCode}")]
        public ActionResult AddVehicle(int typeCode)
        {
            try
            {
                this.parking.AddVehicle(VehicleFactory.CreateVehicle(typeCode));
                return (ActionResult)Ok();
            }
            catch (NotEnoughPlacesException)
            {
                return (ActionResult)BadRequest();
            }
            catch (InvalidVehicleTypeCodeException)
            {
                return (ActionResult)BadRequest();
            }
        }

        // PATCH: api/parking/vehicle/topup?id={id}&money={money}
        [HttpPatch("id, money")]
        [Route("vehicle/topup")]
        public ActionResult TopUpBalance(int id, decimal money)
        {
            try
            {
                var result = this.parking.GetVehicleById(id);
                result.TopUpBalance(money);

                return (ActionResult)Ok();
            }
            catch (InvalidVehicleIdException)
            {
                return (ActionResult)BadRequest();
            }
        }
    }
}
