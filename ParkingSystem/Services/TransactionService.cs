﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using ParkingSystem.Models;

namespace ParkingSystem.Services
{
    public class TransactionService
    {
        public string TransactionLogFile { get; set; }
        public List<Transaction> Transactions { get; private set; }

        public TransactionService()
        {
            this.Transactions = new List<Transaction>();
            this.TransactionLogFile = Settings.TransactionLogFile;
        }

        public void Add(Transaction transaction)
        {
            this.Transactions.Add(transaction);
        }

        public void ClearHistory()
        {
            this.Transactions.Clear();
        }

        public void WriteToFile()
        {
            using (var tw = new StreamWriter(this.TransactionLogFile, true))
            {
                foreach (Transaction transaction in this.Transactions)
                {
                    tw.WriteLine(transaction.ToString());
                }
            }
        }

        public decimal Earnings()
        {
            return this.Transactions.Sum(t => t.MoneyAmount);
        }

        public string[] GetAllTransactions()
        {
            try
            {
                return File.ReadAllLines(this.TransactionLogFile);
            }
            catch (FileNotFoundException)
            {
                return new string[] { "There are no transactions yet. (Transactions.log doesn't exist)" };
            }
        }
    }
}