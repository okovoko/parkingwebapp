﻿using System.Collections.Generic;

namespace ParkingSystem
{
    public static class Settings
    {
        public static readonly decimal DefaultParkingBalance = 0M;
        public static readonly int ParkingCapacity = 10;
        public static readonly int PaymentPeriod = 5000; // ms
        public static readonly decimal FeePenaltyRatio = 2.5M;
        public static readonly Dictionary<string, decimal> Prices = new Dictionary<string, decimal>()
        {
            [typeof(Models.Car).Name] = 2M,
            [typeof(Models.Bus).Name] = 3.5M,
            [typeof(Models.Truck).Name] = 5M,
            [typeof(Models.Motorcycle).Name] = 1M
        };
        public static readonly decimal DefaultVehicleBalance = 15;

        public static readonly int LogPeriod = 60000; // ms
        public static readonly string TransactionLogFile = "Transactions.log";
    }
}