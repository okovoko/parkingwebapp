﻿using System;

namespace ParkingSystem.Exceptions
{
    public class NotEnoughPlacesException : Exception
    {
        public NotEnoughPlacesException()
        {
        }

        public NotEnoughPlacesException(string message)
            : base(message)
        {
        }

        public NotEnoughPlacesException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
