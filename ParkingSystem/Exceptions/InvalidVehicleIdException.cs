﻿using System;

namespace ParkingSystem.Exceptions
{
    public class InvalidVehicleIdException : Exception
    {
        public InvalidVehicleIdException()
        {
        }

        public InvalidVehicleIdException(string message)
            : base(message)
        {
        }

        public InvalidVehicleIdException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
