﻿using System;
using System.Collections.Generic;
using System.Timers;
using ParkingSystem.Abstractions;
using ParkingSystem.Exceptions;
using ParkingSystem.Services;

namespace ParkingSystem.Models
{
    public sealed class Parking : IDisposable
    {
        private TimeService _timeService;

        public decimal Balance { get; private set; }
        public int Capacity { get; private set; }
        public Dictionary<string, decimal> VehicleTypeFee { get; private set; }
        public List<Vehicle> VehicleList { get; private set; }
        public TransactionService TransactionService { get; private set; }


        public Parking()
        {
            this.Balance = Settings.DefaultParkingBalance;
            this.Capacity = Settings.ParkingCapacity;
            this.VehicleTypeFee = Settings.Prices;
            this.VehicleList = new List<Vehicle>();

            this.TransactionService = new TransactionService();
            InitializeTimeService();
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (this.Capacity <= this.VehicleList.Count)
                throw new NotEnoughPlacesException("Parking is full. You cannot add another vehicle.");
            this.VehicleList.Add(vehicle);
        }

        public int VehicleIndex(int vehicleId) =>
            VehicleList.FindIndex(v => v.Id == vehicleId);

        public void RemoveVehicle(int vehicleId)
        {
            int vehicleIndex = VehicleIndex(vehicleId);
            if (vehicleIndex != -1)
            {
                this.VehicleList.RemoveAt(vehicleIndex);
            }
            else
            {
                throw new InvalidVehicleIdException("Invalid vehicle Id.");
            }
        }

        public Vehicle GetVehicleById(int vehicleId)
        {
            int vehicleIndex = VehicleIndex(vehicleId);
            if (vehicleIndex != -1)
            {
                return this.VehicleList[vehicleIndex];
            }
            else
            {
                throw new InvalidVehicleIdException("Invalid vehicle Id.");
            }
        }

        private void InitializeTimeService()
        {
            Dictionary<ElapsedEventHandler, int> Events = new Dictionary<ElapsedEventHandler, int>();

            Events.Add((obj, eArgs) =>
            {
                this.TransactionService.WriteToFile();
                this.TransactionService.ClearHistory();
            },
            Settings.LogPeriod);

            Events.Add((obj, eArgs) =>
            {
                foreach (Vehicle vehicle in this.VehicleList)
                {
                    decimal price = this.VehicleTypeFee[vehicle.GetType().Name];
                    decimal fee = vehicle.Balance < price ? price * Settings.FeePenaltyRatio : price;

                    vehicle.WithdrawBalance(fee);
                    this.Balance += fee;

                    this.TransactionService.Add(new Transaction(vehicle.Id, fee));
                }
            },
            Settings.PaymentPeriod);

            this._timeService = new TimeService(Events);
        }

        public void Dispose()
        {
            this._timeService.Dispose();
        }
    }
}