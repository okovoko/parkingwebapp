﻿using System;
namespace ParkingSystem.Models

{
    public class Transaction
    {
        private readonly System.DateTime _transactionTime;
        private readonly int _vehicleId;

        public decimal MoneyAmount { get; private set; }

        public Transaction(int vehicleId, decimal moneyAmount)
        {
            this._transactionTime = DateTime.Now;
            this._vehicleId = vehicleId;
            this.MoneyAmount = moneyAmount;
        }

        public override string ToString()
        {
            return $"Time {this._transactionTime}\tVehicle Id: {this._vehicleId}\t Amount of money: {this.MoneyAmount}";
        }
    }
}