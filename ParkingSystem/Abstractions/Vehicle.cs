﻿using System;

namespace ParkingSystem.Abstractions
{
    abstract public class Vehicle
    {
        static int LastId = 0;

        public int Id { get; }
        public decimal Balance { get; private set; }
        public DateTime ParkingStartTime { get; }

        public Vehicle()
            : this(Settings.DefaultVehicleBalance)
        {
        }

        public Vehicle(decimal balance)
        {
            this.Id = ++LastId;
            this.Balance = balance;
            this.ParkingStartTime = DateTime.Now;
        }

        public void TopUpBalance(decimal moneyAmount)
        {
            this.Balance += moneyAmount;
        }

        public void WithdrawBalance(decimal moneyAmount)
        {
            this.Balance -= moneyAmount;
        }

        public override string ToString()
        {
            return $"Id: {this.Id}\tType: {this.GetType().Name}\tBalance: {this.Balance}";
        }
    }
}