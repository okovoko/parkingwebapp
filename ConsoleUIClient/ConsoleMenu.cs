﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ConsoleUIClient
{
    public class ConsoleMenu : IDisposable
{
        private System.Net.Http.HttpClient client;
        private string baseURL;
        bool exit;

        public ConsoleMenu()
        {
            client = new System.Net.Http.HttpClient();
            baseURL = @"https://localhost:44367/";
        }

        private void WriteLine(string text, ConsoleColor consoleColor = ConsoleColor.Yellow)
        {
            Console.ForegroundColor = consoleColor;
            Console.WriteLine(text);
            Console.ResetColor();
        }

        public void Run()
        {
            while (!exit)
            {
                ShowMenuItems();
                int userInput = GetInput();
                Perform(userInput);
            }
        }

        private void Perform(int userInput)
        {
            switch (userInput)
            {
                case 0:
                    DisplayParkingBalance();
                    break;
                case 1:
                    DisplayLastMinuteEarnings();
                    break;
                case 2:
                    DisplayFreeAndOccupiedPlacesAmount();
                    break;
                case 3:
                    DisplayLastMinuteTransactions();
                    break;
                case 4:
                    DisplayAllTransaction();
                    break;
                case 5:
                    DisplayVehicleList();
                    break;
                case 6:
                    AddVehicle();
                    break;
                case 7:
                    RemoveVehicle();
                    break;
                case 8:
                    TopUpVehicleBalance();
                    break;
                case 9:
                    Exit();
                    break;

                default:
                    WriteLine("Please, Enter the number from 0 to 9.", ConsoleColor.Red);
                    break;
            }

            Console.WriteLine("Press enter");
            Console.ReadLine();
        }


        private int GetInput()
        {
            int userInput;
            do
            {
                WriteLine("Please, Enter your choice as a number:");
            } while (!int.TryParse(Console.ReadLine(), out userInput));

            return userInput;
        }

        private void ShowMenuItems()
        {
            Console.Clear();

            WriteLine("Choose option");

            Console.WriteLine();

            WriteLine("0 - Display parking balance");
            WriteLine("1 - Display the amount of money earned in the last minute");
            WriteLine("2 - Display the number of free and busy places at the parking");
            WriteLine("3 - Display parking transactions in the last minute");
            WriteLine("4 - Display the transaction history");
            WriteLine("5 - Display the list of all vehicles");
            WriteLine("6 - Park the vehicle");
            WriteLine("7 - Remove the vehicle");
            WriteLine("8 - Top up the balance of the vehicle");
            WriteLine("9 - Exit");

            Console.WriteLine();
        }

        private void DisplayParkingBalance()
        {
            WriteLine("Parking balance:");
            WriteLine(client.GetStringAsync(baseURL + @"api/parking/balance").Result);
        }

        private void DisplayLastMinuteEarnings()
        {
            WriteLine("Money earned in last minute:");
            WriteLine(client.GetStringAsync(baseURL + @"api/parking/earnings").Result);
        }

        private void DisplayFreeAndOccupiedPlacesAmount()
        {
            var result = JsonConvert
                .DeserializeObject<(int, int)>(client.GetStringAsync(baseURL + @"api/parking/places").Result);
            WriteLine($"Amount of free places = {result.Item1}");
            WriteLine($"Amount of occupied places = {result.Item2}");
        }

        private void DisplayLastMinuteTransactions()
        {
            WriteLine("Last transactions:");
            var lastTransactions = JsonConvert
                .DeserializeObject<List<string>>(client.GetStringAsync(baseURL + @"api/parking/transactions/last").Result);

            foreach (var transaction in lastTransactions)
            {
                WriteLine(transaction);
            }
        }

        private void DisplayAllTransaction()
        {
            WriteLine("All transactions:");
            var lastTransactions = JsonConvert
                .DeserializeObject<string[]>(client.GetStringAsync(baseURL + @"api/parking/transactions/all").Result);

            foreach (var transaction in lastTransactions)
            {
                WriteLine(transaction);
            }

        }

        private void DisplayVehicleList()
        {
            WriteLine("Vehicles at parking:");

            var result = JsonConvert.
                DeserializeObject<List<string>>(client.GetStringAsync(baseURL + @"api/parking/vehicle/all").Result);

            foreach (var vehicle in result)
            {
                WriteLine(vehicle);
            }
        }

        private void AddVehicle()
        {
            WriteLine("Vehicle types:");
            ShowVehicleType();
            Console.WriteLine();

            int vehicleTypeCode = -1;
            do
            {
                WriteLine("Enter vehicle type as a number:");
            } while (!int.TryParse(Console.ReadLine(), out vehicleTypeCode));

            var result = client.PutAsync(baseURL + $@"api/parking/vehicle/{vehicleTypeCode}", new System.Net.Http.StringContent(String.Empty))
                            .Result;

            bool isAdded = result.IsSuccessStatusCode;

            if (isAdded)
                WriteLine("New vehicle is added.", ConsoleColor.Green);
            else
                WriteLine("Vehicle isn't added.", ConsoleColor.Red);

        }
        private void ShowVehicleType()
        {
            WriteLine("0 - Car");
            WriteLine("1 - Truck");
            WriteLine("2 - Bus");
            WriteLine("3 - Motorcycle");
        }

        private void RemoveVehicle()
        {
            int vehicleId;
            do
            {
                WriteLine("Enter vehicle id:");
            } while (!int.TryParse(Console.ReadLine(), out vehicleId));

            bool isRemoved = client.DeleteAsync(baseURL + $@"api/parking/vehicle/{vehicleId}").Result.IsSuccessStatusCode;

            if (isRemoved)
                WriteLine("Vehicle is deleted", ConsoleColor.Green);
            else
                WriteLine("Vehicle is not deleted", ConsoleColor.Red);
        }

        private void TopUpVehicleBalance()
        {
            int vehicleId;
            do
            {
                Console.WriteLine("Enter vehicle id:");
            } while (!int.TryParse(Console.ReadLine(), out vehicleId));

            decimal moneyAmount;
            do
            {
                Console.WriteLine("Enter money amount:");
            } while (!decimal.TryParse(Console.ReadLine(), out moneyAmount));

            var result = client
                .PatchAsync(baseURL + $@"api/parking/vehicle/topup?id={vehicleId}&money={moneyAmount}", new System.Net.Http.StringContent(String.Empty))
                .Result;

            bool isToppedUp = result.IsSuccessStatusCode;

            if (isToppedUp)
                WriteLine("Topped up successfully.", ConsoleColor.Green);
            else
                WriteLine("Balance does not topped up.", ConsoleColor.Red);
        }

        private void Exit()
        {
            exit = true;
        }

        public void Dispose()
        {
            client.Dispose();
        }
    }
}