﻿using System;

namespace ConsoleUIClient
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ConsoleMenu menu = new ConsoleMenu())
            {
                menu.Run();
            }
        }
    }
}
